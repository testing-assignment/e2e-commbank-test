# E2E Commbank Test

[![E2E Commbank Test](https://img.shields.io/endpoint?url=https://dashboard.cypress.io/badge/simple/xeusyu&style=flat&logo=cypress)](https://dashboard.cypress.io/projects/xeusyu/runs)


## Getting Started

## Pre-rquisite 
- Node 12.18.3
- Chrome 87
- Firefox

### 1. Clone the repo
### 2. Run below command in root directory:
`npm install`

`npx cypress open`
or 
`./node_modules/.bin/cypress open`

running test in command line:

`./node_modules/.bin/cypress run --browser chrome`

### 3.You will see a cypress runner is open and you can select the feature files and run the tests.

more info: https://docs.cypress.io/guides/core-concepts/test-runner.html#Overview

## Build Pipeline

**To run test please trigger the pipeline using**

https://gitlab.com/testing-assignment/e2e-commbank-test/-/pipelines

Login to gitlab with provided username/password.

click on Run Pipeline
- Default variables are set here https://gitlab.com/testing-assignment/e2e-commbank-test/-/settings/ci_cd
- Override the varibles to run in firefox or change the viewport. 


## Test Execution report on Cypress Dashboard

you can see the test run screenshot and videos and other test statistics on cypress dashboard:

https://dashboard.cypress.io/projects/xeusyu/runs

