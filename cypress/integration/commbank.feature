Feature: Commbank website top navigation

  As a user I want to click top nav tabs so that I can use different products.

Background: Desktop viewport
  Scenario Outline: Clicking on tabs of the top header.
    Given I open commbank home page
    When I click "<tab name>"
    Then I should see tab open with "<tab text>".
    Examples:
      | tab name      | tab text                          |
      | Banking       | Banking                           |
      | Home loans    | Home loans                        |
      | Insurance     | Insurance                         |
      | Investing     | Investing & Super                 |
      | Institutional | Institutional banking and markets |