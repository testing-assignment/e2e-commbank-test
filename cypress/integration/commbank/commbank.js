import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';
import * as homePage from "../../pages/commbank/homePage";

Given('I open commbank home page', () => {
  homePage.open();
});

When('I click {string}', (tabName) => {
  homePage.clickTheHeaderBar(tabName)});

Then('I should see tab open with {string}.', (tabText) => {
  homePage.verifyThePageContains(tabText);
});



