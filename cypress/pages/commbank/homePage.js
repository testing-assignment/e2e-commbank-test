const departDate = '#FlightsDateStart';
const currentDate ='#datepickers-container > .active .-current-';
const searchButton = '#flights [type=submit].btn-primary';

export function open() {
    cy.visit("https://www.commbank.com.au/");
    handleViewport();
}

export function clickTheHeaderBar(tabName) {
    cy.get('.commbank-header').contains(tabName).click({force: true});
}

export function verifyThePageContains(tabText) {
    cy.get('h1').contains(tabText).should('be.visible');
}

function handleViewport() {
    cy.get('.hamburger-menu-icon').then((x) => {
      if (x.ariaHidden) {
        x.click();
        cy.log("Hamburger was visible and clicked.");
      }
    });
}
